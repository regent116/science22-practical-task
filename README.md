## Installation


`
	git clone https://regent116@bitbucket.org/regent116/science22-practical-task.git && cd science22-practical-task && npm i
`
---

## Demo


[http://jammetti.lv:81/](http://jammetti.lv:81/)


---

## Preview

![picture](https://i.imgur.com/SADe2BE.gif)

Functional **Dad Joke Generator**

1. Mobile friendly
2. Resizable
3. Used axios for GET request. Implemented MVC pattern
4. Built my own setup enviroment (webpack) eslint and prettier
5. Hosted on raspberry pi

![picture](https://i.imgur.com/Q3fTzaA.gif)


Created by Pāvels Jacuks.