import React, { PureComponent } from 'react';
import { Helmet } from 'react-helmet';
import './App.style.css';
import Generator from '../Generator';
import { animateIntro } from '../Animation.js';

export default class App extends PureComponent {
	constructor(props) {
		super(props);
		this.dadImg = React.createRef();
		this.generator = React.createRef();
	}

	componentDidMount() {
		animateIntro(
			this.generator.container,
			this.generator.title,
			this.generator.button,
			this.generator.prompt,
			this.dadImg
		);
	}

	render() {
		return (
			<div className="background">
				<Helmet>
					<meta name="viewport" content="width=device-width, initial-scale=1.0" />
				</Helmet>
				<div className="base-container">
					<div className="dad-img-container" ref={(e) => {this.dadImg = e;}}>
						<img alt="joker dad" src="https://i.imgur.com/Vp5oZ33.png" />
					</div>
					<Generator ref={(e) => {this.generator = e;}}/>
				</div>
			</div>
		);
	}
}
