import React, { PureComponent } from 'react';
import './Generator.style.css';
import gsap from 'gsap';
import { TextPlugin } from 'gsap/TextPlugin';
import Button from '../Button';
import { animateJokeContainer, animatePreviousJokeContainer } from '../Animation.js';
import { Controller } from '../Controller.js';
import { setArrayLimit } from '../ArrayLimit.js';

const jokeArrayLimit = 6;

export default class Generator extends PureComponent {
	constructor(props) {
		super(props);
		this.state = {
			jokes: [],
			className: 'spinner inactive',
			hoverClassName: 'button active2'
		};
		this.onSubmit = this.onSubmit.bind(this);
		this.container = React.createRef();
		this.title = React.createRef();
		this.button = React.createRef();
		this.prompt = React.createRef();
		this.joke = React.createRef();
		this.spinner = React.createRef();
		this.previousJoke = React.createRef();
	}

	componentDidMount() {
		gsap.registerPlugin(TextPlugin);
	}

	onSubmit() {
		const { jokes } = this.state;
		this.setState({ className: 'spinner active', hoverClassName: 'button inactive2' });
		Controller().then((res) => {
			jokes.unshift(res);
			this.forceUpdate();
			animateJokeContainer(this.joke, jokes[0]);
			animatePreviousJokeContainer(this.previousJoke);
			this.setState({
				className: 'spinner inactive2',
				hoverClassName: 'button active2',
				jokes: setArrayLimit(jokes, jokeArrayLimit)
			});
			this.forceUpdate();
		});
	}

	render() {
		const { jokes, className, hoverClassName } = this.state;
		const jokeSlice = jokes.slice(1, jokeArrayLimit);
		
		return (
			<div className="generator-container" ref={(e) => {this.container = e;}}>
				<h1	ref={(e) => {this.title = e;}}>
					Want to hear a dad joke?
				</h1>
				<Button
				  value="Give it to me!"
				  hoverClassName={hoverClassName}
				  submit={this.onSubmit}
				  ref={(e) => {this.button = e;}}
				/>
				<div className="single-joke-container" ref={(e) => {this.joke = e;}}/>
				<div className="joke-container" ref={(e) => {this.prompt = e;}}>
					<div name="loading-spinner" className={className} ref={(e) => {this.spinner = e;}}/>
					<h2> Previous dad jokes: </h2>
					<div ref={(e) => {this.previousJoke = e;}}>
						{jokes.length < 2 ? (
							<p>Nothing yet, ask me it to you</p>
						) : (
							<ul>
								{jokeSlice.map((joke) => (
									<li>{joke}</li>
								))}
							</ul>
						)}
					</div>
				</div>
			</div>
		);
	}
}
