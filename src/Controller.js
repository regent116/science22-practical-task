import Axios from 'axios';

const API_PATH = 'https://icanhazdadjoke.com/';

export const Controller = () => {
	return Axios({
		method: 'get',
		url: API_PATH,
		headers: {
			accept: 'application/json'
		}
	})
		.then((response) => {
			return response.data.joke;
		})
		.catch((error) => {
			return error;
		});
};
